﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WarioSoft12.Models
{
    public class User
    {

        [Key]
        public int id_Document { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Fist_name { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string second_Name { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string first_Lastname { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string second_Lastname { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Document { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notas { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Address { get; set; }
    }
}