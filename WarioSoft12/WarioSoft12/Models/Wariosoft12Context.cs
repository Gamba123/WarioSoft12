﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WarioSoft12.Models
{
    public class Wariosoft12Context: DbContext
    {
        public Wariosoft12Context(): base("DefaultConnection")
        {
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public System.Data.Entity.DbSet<WarioSoft12.Models.User> Users { get; set; }
    }
}