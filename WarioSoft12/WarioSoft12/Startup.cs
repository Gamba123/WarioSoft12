﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WarioSoft12.Startup))]
namespace WarioSoft12
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
