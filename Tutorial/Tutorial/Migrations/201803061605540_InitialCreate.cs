namespace Tutorial.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id_Document = c.Int(nullable: false, identity: true),
                        Fist_name = c.String(),
                        second_Name = c.String(),
                        first_Lastname = c.String(),
                        second_Lastname = c.String(),
                        Document = c.String(),
                    })
                .PrimaryKey(t => t.id_Document);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
