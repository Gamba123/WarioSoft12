﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tutorial.Models
{
    public class User
    {
        [Key]
        public int id_Document { get; set; }
        public string Fist_name { get; set; }
        public string second_Name { get; set; }
        public string first_Lastname { get; set; }
        public string second_Lastname { get; set; }
        public string Document { get; set; }
    }
}