﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Tutorial.Models
{
    public class TutorialContext : DbContext
    {
        public TutorialContext() : base("DefaultConnection")
        {

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public System.Data.Entity.DbSet<Tutorial.Models.User> Users { get; set; }

    }
}